#![feature(once_cell)]
use bevy::{
    core::FixedTimestep,
    prelude::*,
    sprite::collide_aabb::{collide, Collision},
};
use std::lazy::SyncLazy;

static DEFAULT_BALL_VELOCITY: SyncLazy<Vec3> =
    SyncLazy::new(|| 300.0 * Vec3::new(0.5, -0.5, 0.0).normalize());
static DEFAULT_BALL_VELOCITY_REVERSE: SyncLazy<Vec3> =
    SyncLazy::new(|| 300.0 * Vec3::new(-0.5, 0.5, 0.0).normalize());

const TIME_STEP: f32 = 1.0 / 60.0;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .insert_resource(Scoreboard::default())
        .add_startup_system(setup)
        .add_system_set(
            SystemSet::new()
                .with_run_criteria(FixedTimestep::step(TIME_STEP as f64))
                .with_system(paddle_movement_system)
                .with_system(ball_collision_system)
                .with_system(ball_movement_system)
                .with_system(hard_paddle_movement_system)
                .with_system(toggle_hard_system),
        )
        .add_system(scoreboard_system)
        .add_system(restart_system)
        .add_system(game_end_system)
        .run();
}

#[derive(Component)]
struct Death;
#[derive(Component)]
struct Restartable;
//struct ScoreSound;

#[derive(Component)]
struct Paddle {
    speed: f32,
    is_hard: bool,
}

#[derive(Default, Component, Debug)]
struct Ball {
    velocity: Vec3,
    score: usize,
}

#[derive(Default, Component)]
struct Scoreboard {
    score: usize,
    high: usize,
}

#[derive(Component)]
enum Collider {
    Solid,
    Scorable,
    Paddle,
    Death,
    Ball,
}

#[derive(Component)]
enum TextKind {
    Score,
    Name,
}

fn setup(mut commands: Commands, asset_server: Res<AssetServer>) {
    // Add the game's entities to our world

    // cameras
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn_bundle(UiCameraBundle::default());

    info!("Paddle");
    // paddle
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb(0.8, 0.8, 0.8),
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(0.0, -215.0, 1.0),
                scale: Vec3::new(120.0, 35.0, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Paddle {
            speed: 500.0,
            is_hard: false,
        })
        .insert(Collider::Paddle);

    info!("Scoreboard");
    // scoreboard
    commands
        .spawn_bundle(TextBundle {
            text: Text::with_section(
                "Score:".to_string(),
                TextStyle {
                    font: asset_server.load("fonts/Hack-Regular.ttf"),
                    color: Color::rgb(0.7, 0.7, 0.9),
                    font_size: 38.0,
                },
                TextAlignment::default(),
            ),
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(5.0),
                    right: Val::Px(5.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(TextKind::Score);
    // A warning text
    commands
        .spawn_bundle(TextBundle {
            text: Text::with_section(
                "Breakout!!!111!11!!!!1".to_string(),
                TextStyle {
                    font: asset_server.load("fonts/Hack-Regular.ttf"),
                    color: Color::rgb(0.7, 0.7, 0.9),
                    font_size: 38.0,
                },
                TextAlignment::default(),
            ),
            style: Style {
                position_type: PositionType::Absolute,
                position: Rect {
                    top: Val::Px(5.0),
                    left: Val::Px(5.0),
                    ..Default::default()
                },
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(TextKind::Name);

    // Add walls
    let wall_color = Color::rgb(0.8, 0.8, 0.8);
    let wall_thickness = 35.0;
    let bounds = Vec2::new(1200.0, 600.0);

    // left
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: wall_color,
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(-bounds.x / 2.0, 0.0, 0.0),
                scale: Vec3::new(wall_thickness, bounds.y + wall_thickness, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Collider::Solid);
    // right
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: wall_color,
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(bounds.x / 2.0, 0.0, 0.0),
                scale: Vec3::new(wall_thickness, bounds.y + wall_thickness, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Collider::Solid);
    // bottom
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: wall_color,
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(0.0, -bounds.y / 2.0, 0.0),
                scale: Vec3::new(bounds.x + wall_thickness, wall_thickness, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Death)
        .insert(Collider::Death);
    // top
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: wall_color,
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(0.0, bounds.y / 2.0, 0.0),
                scale: Vec3::new(bounds.x + wall_thickness, wall_thickness, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Collider::Solid);

    info!("Blocks");
    spawn_blocks(&mut commands);
    info!("Balls");
    spawn_balls(&mut commands);
}

fn spawn_blocks(commands: &mut Commands) {
    let brick_rows = 5;
    let brick_columns = 7;
    let brick_spacing = 15.0;
    let brick_size = Vec3::new(125.0, 25.0, 0.0);
    let bricks_width = brick_columns as f32 * (brick_size.x + brick_spacing) - brick_spacing;

    // center the bricks and move them up a bit
    let bricks_offset = Vec3::new(-(bricks_width - brick_size.x) / 2.0, 100.0, 0.0);
    let brick_color = Color::rgb(0.9, 0.5, 0.5);
    for row in 0..brick_rows {
        let y_position = row as f32 * (brick_size.y + brick_spacing);
        for column in 0..brick_columns {
            let brick_position = Vec3::new(
                column as f32 * (brick_size.x + brick_spacing),
                y_position,
                0.0,
            ) + bricks_offset;
            commands
                // brick
                .spawn_bundle(SpriteBundle {
                    sprite: Sprite {
                        color: brick_color,
                        ..Default::default()
                    },
                    transform: Transform {
                        translation: brick_position,
                        scale: brick_size,
                        ..Default::default()
                    },
                    ..Default::default()
                })
                .insert(Collider::Scorable)
                .insert(Restartable);
        }
    }
}

fn spawn_balls(commands: &mut Commands) {
    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(181, 122, 244),
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(0.0, -125.0, 1.0),
                scale: Vec3::new(30.0, 30.0, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Ball {
            velocity: *DEFAULT_BALL_VELOCITY,
            ..Default::default()
        })
        .insert(Collider::Ball)
        .insert(Restartable);

    commands
        .spawn_bundle(SpriteBundle {
            sprite: Sprite {
                color: Color::rgb_u8(181, 122, 244),
                ..Default::default()
            },
            transform: Transform {
                translation: Vec3::new(0.0, -125.0, 1.0),
                scale: Vec3::new(30.0, 30.0, 0.0),
                ..Default::default()
            },
            ..Default::default()
        })
        .insert(Ball {
            velocity: *DEFAULT_BALL_VELOCITY_REVERSE,
            ..Default::default()
        })
        .insert(Collider::Ball)
        .insert(Restartable);
}

fn paddle_movement_system(
    mut mouse_motion_events: EventReader<CursorMoved>,
    mut query: Query<(&Paddle, &mut Transform)>,
) {
    for (paddle, mut transform) in query.iter_mut() {
        if !paddle.is_hard {
            let translation = &mut transform.translation;

            for event in mouse_motion_events.iter() {
                translation.x = event.position.x - 640.0;
                translation.y = event.position.y - 360.0;
            }

            // bound the paddle within the walls
            translation.x = translation.x.min(530.0).max(-530.0);
            translation.y = translation.y.min(260.0).max(-260.0);
        }
    }
}

fn hard_paddle_movement_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut query: Query<(&Paddle, &mut Transform)>,
) {
    for (paddle, mut transform) in query.iter_mut() {
        if paddle.is_hard {
            let mut direction = 0.0;
            if keyboard_input.pressed(KeyCode::Left) {
                if keyboard_input.pressed(KeyCode::LShift) {
                    if keyboard_input.pressed(KeyCode::LControl) {
                        direction -= 3.0;
                    } else {
                        direction -= 2.0;
                    }
                } else {
                    direction -= 1.0;
                }
            }

            if keyboard_input.pressed(KeyCode::Right) {
                if keyboard_input.pressed(KeyCode::LShift) {
                    if keyboard_input.pressed(KeyCode::LControl) {
                        direction += 3.0;
                    } else {
                        direction += 2.0;
                    }
                } else {
                    direction += 1.0;
                }
            }

            let translation = &mut transform.translation;
            // move the paddle horizontally
            translation.x += TIME_STEP * direction * paddle.speed;
            // bound the paddle within the walls
            translation.x = translation.x.min(530.0).max(-530.0);
        }
    }
}

fn ball_movement_system(mut ball_query: Query<(&Ball, &mut Transform)>) {
    for (ball, mut transform) in ball_query.iter_mut() {
        transform.translation += ball.velocity * TIME_STEP;
    }
}

fn scoreboard_system(scoreboard: Res<Scoreboard>, mut query: Query<(&mut Text, &TextKind)>) {
    for (mut text, kind) in query.iter_mut() {
        if let TextKind::Score = *kind {
            text.sections[0].value = format!(
                "Hi-Score: {} - Score: {}",
                scoreboard.high, scoreboard.score
            );
        }
    }
}

fn ball_collision_system(
    mut commands: Commands,
    mut scoreboard: ResMut<Scoreboard>,
    mut ball_query: Query<(&mut Ball, &Transform)>,
    collider_query: Query<(Entity, &Collider, &Transform)>,
    //asset_server: Res<AssetServer>,
    //audio: Res<Audio>,
) {
    //let effect = asset_server.load("sounds/score.mp3");

    for (mut ball, ball_transform) in ball_query.iter_mut() {
        let ball_size = ball_transform.scale.truncate();

        // check collision with walls
        for (collider_entity, collider, transform) in collider_query.iter() {
            let collision = collide(
                ball_transform.translation,
                ball_size,
                transform.translation,
                transform.scale.truncate(),
            );

            if let Some(collision) = collision {
                if let Collider::Death = *collider {
                    scoreboard.score = 0;
                    ball.score = 0;
                    ball.velocity = 300.0 * ball.velocity.normalize();
                }

                // scorable colliders should be despawned and increment the scoreboard on collision
                if let Collider::Scorable = *collider {
                    scoreboard.score += 1;

                    if scoreboard.high < scoreboard.score {
                        scoreboard.high = scoreboard.score;
                    }

                    ball.velocity =
                        (300.0 + (40.0 * ball.score as f32)) * ball.velocity.normalize();
                    ball.score += 1;

                    commands.entity(collider_entity).despawn();

                    //audio.play(effect.clone());
                }

                // reflect the ball when it collides
                let mut reflect_x = false;
                let mut reflect_y = false;

                // only reflect if the ball's velocity is going in the opposite direction of the collision
                match collision {
                    Collision::Left => reflect_x = ball.velocity.x > 0.0,
                    Collision::Right => reflect_x = ball.velocity.x < 0.0,
                    Collision::Top => reflect_y = ball.velocity.y < 0.0,
                    Collision::Bottom => reflect_y = ball.velocity.y > 0.0,
                }

                // reflect velocity on the x-axis if we hit something on the x-axis
                if reflect_x {
                    ball.velocity.x = -ball.velocity.x;
                }

                // reflect velocity on the y-axis if we hit something on the y-axis
                if reflect_y {
                    ball.velocity.y = -ball.velocity.y;
                }

                // break if this collide is on a solid, otherwise continue check whether a solid is also in collision
                match *collider {
                    Collider::Solid | Collider::Death | Collider::Ball => break,
                    _ => (),
                }
            }
        }
    }
}

fn restart_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut commands: Commands,

    mut scoreboard: ResMut<Scoreboard>,
    mut ball_query: Query<(&mut Ball, &Restartable, Entity)>,
    mut blocks_query: Query<(&mut Collider, &Restartable, Entity)>,
) {
    if keyboard_input.just_pressed(KeyCode::R) {
        for (_, _, e) in ball_query.iter_mut() {
            info!("{:?}", e);
            commands.entity(e).despawn();
        }

        for (collider, _, e) in blocks_query.iter_mut() {
            if let Collider::Ball = *collider {
            } else {
                commands.entity(e).despawn();
            }
        }

        scoreboard.score = 0;

        spawn_blocks(&mut commands);
        spawn_balls(&mut commands);
    }
}

fn toggle_hard_system(
    keyboard_input: Res<Input<KeyCode>>,
    mut paddles: Query<(&mut Paddle, &mut Transform)>,
) {
    if keyboard_input.just_pressed(KeyCode::H) {
        for (mut paddle, mut transform) in paddles.iter_mut() {
            paddle.is_hard = !paddle.is_hard;

            let translation = &mut transform.translation;
            translation.y = -215.0;
        }
    }
}

fn game_end_system(mut commands: Commands, blocks_query: Query<(&Collider, &Restartable)>) {
    let x = blocks_query
        .iter()
        .filter_map(|i| {
            if let Collider::Scorable = i.0 {
                Some(i)
            } else {
                None
            }
        })
        .count();

    if x == 0 {
        spawn_blocks(&mut commands);
    }
}
