#!/bin/sh

project_name='inkball'

if [ "$1" = "--release" ]; then
    (
        set -x

        cp static/index.html static_release/
        cp static/*.css static_release/
        cp static/link.svg static_release/
    )

    path="static_release/target"
else
    path="static/target"
fi

(
    set -x
    
    cargo build --release --target wasm32-unknown-unknown || exit
    wasm-bindgen "target/wasm32-unknown-unknown/release/${project_name}.wasm" --target web --out-dir ${path} --no-typescript || exit
    #wasm-opt ${path}/${project_name}_bg.wasm -O3 --fast-math -o ${path}/${project_name}_bg.wasm || exit
)
